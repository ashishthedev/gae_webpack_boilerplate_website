https://www.mugo.ca/Blog/How-to-customize-Bootstrap-4-using-Sass

# How to customize Bootstrap 4 using Sass
By: Carlos Mauri | January 18, 2019 | Front-end development and User experience

Some months ago I listed 7 reasons(https://www.mugo.ca/Blog/7-benefits-of-using-SASS-over-conventional-CSS) why you should be using Sass over conventional CSS to build stunning websites. One of these reasons is the ability to customize Bootstrap, the most used front-end framework in the world. In this blog post, I will explain some basic concepts to enhance Bootstrap 4 with Sass to deliver a unique and delightful user experience.



## Sass and Bootstrap 4 setup

To get started, install Sass and get the latest Bootstrap 4 distribution. The current latest version of Bootstrap is 4.2.1. You'll need the Bootstrap 4 source files(https://getbootstrap.com/docs/4.2/getting-started/download/#source-files), not to be confused with the compiled CSS/JS files. Once you have unzipped the files, you will find an scss directory. 

## What's inside the scss folder?
The Bootstrap scss folder contains a set of .scss files and also 2 directories, mixins and utilities, with more Sass code. Copy the contents of this folder to a new bootstrap folder and place it within an scss directory underneath the root of your project.

```
my-web-project/ (root folder of your project)
|-- scss/
| |-- bootstrap/
| | |-- mixins/
| | | |-- [...more scss files]
| | |-- utilities/
| | | |-- [...more scss files]
| | |-- _alert.scss
| | |-- _badge.scss
| | |-- _breadcrumb.scss 
| | |-- [...more scss files]
| | |-- _variables.scss
| | |-- bootstrap-grid.scss
| | |-- bootstrap-reboot.scss
| | |-- bootstrap.scss
```

To convert those .scss files into a CSS stylesheet, tell Sass to compile bootstrap.scss into bootstrap.css by running the following command from the root of your project:
```sh
$ sass scss/bootstrap/bootstrap.scss:css/bootstrap.css
```

You can also add the --watch flag to tell Sass to auto-compile the CSS each time a scss file has been updated. Read the Sass guide for further information.

Note that:

css is the folder containing the CSS file(s). You can name it whatever you want.
If the output folder does not exist, the compiler will create it for you.
If something goes wrong, the compiler will show you what happened. For example, this error tells you that a referenced .scss file is missing:
Error: Can't find stylesheet to import.
@import 'je/slider';
        ^^^^^^^^^^^
scss/_je-theme.scss 8:9 @import
scss/je.scss 12:9 root stylesheet
Before starting the customization phase, here are some important principles to follow regarding the Bootstrap Sass source files:

Do not edit Bootstrap source files, because this will make it harder to upgrade your Bootstrap version in the future. 
The bootstrap.scss file contains what .scss files need to be compiled. If you have a look into that file you will see a list of @import references to pick up small Sass pieces. In other words, this is the main file that controls what components you will add.
Another important .scss file is _variables.scss. Every single variable Bootstrap uses is defined here, and there are a lot. Also note that each variable has the string "!default" at the end, meaning you will be able to override their values.
_functions.scss and _mixins.scss are the other 2 main .scss files within the source code. These files contain a set of required functions and mixins that Bootstrap needs in order to compile the other components.
Add your own theme files
How do you customize Bootstrap without editing its files? First, create your own "theme" directory within the scss directory. This folder will contain all your custom scss files. Second, create an empty main.scss file under the scss directory. And third, create a new bootstrap-overrides.scss file, also under the scss directory.

At Mugo, we prefer to use a separate theme folder to keep things organized like this:

my-web-project/ (root folder of your project)
|-- scss/ (scss files)
| |-- bootstrap/ (Bootstrap source files)
| | |-- [...Bootstrap core files]
| |-- my-theme/ (your custom scss files)
| | |-- my-theme.scss
| | |-- [...more scss files and directories if needed]
| |-- bootstrap-overrides.scss
| |-- main.scss
main.scss, as per its name, will be the main scss file in your project. Here, you will tell Sass what needs to be imported:

@import "bootstrap-overrides";
@import "my-theme/my-theme";
bootstrap-overrides.scss will be act like the main bootstrap.scss file. Here, you will have full control over what core Bootstrap files needs to be included, and you can also override the default framework variables.

To start, you can create bootstrap-overrides.scss as a copy of bootstrap/bootstrap.scss, but remember to update the path of each @import statement since we've customized the scss directory structure. 

/*!
* Bootstrap v4.2.1 (https://getbootstrap.com/)
* Copyright 2011-2018 The Bootstrap Authors
* Copyright 2011-2018 Twitter, Inc.
* Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
*/

@import "bootstrap/functions";
@import "bootstrap/variables";
@import "bootstrap/mixins";
@import "bootstrap/root";
@import "bootstrap/reboot";
[... more @import statements...]
Update theme colors
Bootstrap uses a bunch of variables to set the framework color system. All of these variables are located in _variables.scss:

$blue: #007bff !default;
$indigo: #6610f2 !default;
$purple: #6f42c1 !default;
$pink: #e83e8c !default;
$red: #dc3545 !default;
$orange: #fd7e14 !default;
$yellow: #ffc107 !default;
$green: #28a745 !default;
$teal: #20c997 !default;
$cyan: #17a2b8 !default;

[...]

$primary: $blue !default;
$secondary: $gray-600 !default;
$success: $green !default;
$info: $cyan !default;
$warning: $yellow !default;
$danger: $red !default;
$light: $gray-100 !default;
$dark: $gray-800 !default;
Supposing that you want to update the primary theme color (blue), define it in your bootstrap-overrides.scss file. The variable must be placed before all the @import statements.

$primary: #20c997; // your custom primary color

@import "bootstrap/functions";
@import "bootstrap/variables";
@import "bootstrap/mixins";
[...]
When you compile the Sass code, this new $primary color value will be used for all code that references the $primary variable.

Modify the default breakpoints
Bootstrap breakpoints are also defined in the _variables.scss file. Here are the default values:

$grid-breakpoints: map-merge(
(
xs: 0,
sm: 576px,
md: 768px,
lg: 992px,
xl: 1200px
),
$grid-breakpoints
);
To update the existing breakpoints or add new ones, you just need to override the $grid-breakpoints variable passing only the required keys.

$grid-breakpoints: (
sm: 500px,
xxl: 1440px
);
In this example, we are changing the "sm" breakpoint value to 500px (instead of 576px) and adding a new breakpoint named "xxl".

Related to this, there's another important variable named $container-max-widths which defines the maximum width of the .container class per breakpoint. The default values are:

sm: 540px,
md: 720px,
lg: 960px,
xl: 1140px
For our example, we need to update the "sm" value and add a new one for the new "xxl" breakpoint. Otherwise the container class will have a max width of 1140px no matter how big the viewport.

$container-max-widths: (
sm: 480px,
xxl: 1400px
);
Customize the spacing rules
Another great set of Bootstrap helper classes are the ones related to the spacing rules. The default values are managed by the $spacer and $spacers variables. To update their default values, you just need to define them in the bootstrap-overrides.scss file:

$spacer: 1rem;
$spacers:
(
1: ($spacer * .5),
2: $spacer,
3: ($spacer * 1.5),
4: ($spacer * 2),
5: ($spacer * 3),
6: ($spacer * 5)
);
After the Sass code is compiled, it will produce the following CSS:

.m-0 {
margin: 0 !important;
}
[...]
.m-1 {
margin: 0.5rem !important;
}
[...]
.m-2 {
margin: 1rem !important;
}
[...]
.m-3 {
margin: 1.5rem !important;
}
[...]
.m-4 {
margin: 2rem !important;
}
[...]
.m-5 {
margin: 3rem !important;
}
[...]
// new one
.m-6 {
margin: 5rem !important;
}
[...]
Take advantage of some useful variables
Theme colors, breakpoints, and spacing variables are just some of the most useful variables within the framework but there are much more. For example, don't like the rounded borders styling? You can easily remove the rounded-borders by setting the $enable-rounded global variable to "false". Nice, right?

Here are some more useful variables that make some simple but powerful changes to the compiled CSS:

$enable-caret: true !default;
$enable-rounded: true !default;
$enable-shadows: false !default;
$enable-gradients: false !default;
$enable-transitions: true !default;
$enable-prefers-reduced-motion-media-query: true !default;
$enable-grid-classes: true !default;
$enable-print-styles: true !default;
$enable-validation-icons: true !default;
$enable-caret is used to manage the caret icon displayed mainly within the dropdown elements. Default value is true.
$enable-rounded is used to control the rounded borders of elements. If displaying rounded borders you can also manage the border-radius value overriding the $border-radius variable.
$enable-shadows is used to control the box-shadow property of buttons and form elements. $btn-box-shadow is the variable that controls the box-shadow styling.
$enable-gradients enables predefined gradients via background-image styles on various components.
I recommend consulting the Bootstrap Sass options page and also reviewing _variables.scss to see all that you can do.

Use only what you really need
And last but not least, you have full control over what Bootstrap components should be included. If there are Bootstrap components and utilities that your site does not use, you can exclude them so that the compiled CSS file is as light as possible. If you need to use the components in the future, simply uncomment them.

@import "bootstrap/pagination";
// @import "bootstrap/badge";
// @import "bootstrap/jumbotron";
@import "bootstrap/alert";
// @import "bootstrap/progress";
@import "bootstrap/media";
@import "bootstrap/list-group";
@import "bootstrap/close";
// @import "bootstrap/toasts";
@import "bootstrap/modal";
// @import "bootstrap/tooltip";
@import "bootstrap/popover";
@import "bootstrap/carousel";
// @import "bootstrap/spinners";
@import "bootstrap/utilities";
Once you have finished the customization, remember to compile the Sass code to create your CSS file.

$ sass scss/main.scss:css/my-theme.css
Or even better, use the --watch flag so that Sass will compile your scss files in the background every time a file has been updated:

$ sass --watch scss/main.scss:css/my-theme.css
What's next?
The steps above will allow you to customize Bootstrap 4 and modify the standard framework shell a little bit, but this is just the beginning of the customization process. Your site will likely be recognizable as a "Bootstrap site". At Mugo, we make completely unique looking sites as per design specs, delivering delightful user experiences and high performance sites. The starting point is often Bootstrap using the principles outlined above, but we get more out of Sass and Bootstrap by further breaking out our own custom elements, further defining a file and folder structure, and using all of the mixins, utilities, and other framework elements within our theme files (starting in my-theme.scss) as necessary.

Have questions, doubts, or concerns about Bootstrap 4 customization? Leave us a comment or reach out to us anytime. We would love to chat! 