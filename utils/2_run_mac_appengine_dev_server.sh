#!/bin/bash

#Use this script to run app server directly from mac machine.
echo "_______________________________________"
echo "Backend App on port 8052"
echo "_______________________________________"

dev_appserver.py ../app.yaml --port 8052 --admin_port 8053 --host 0.0.0.0 --admin_host 0.0.0.0 --storage_path /tmp/jatinglass --skip_sdk_update_check --google_analytics_client_id= --watcher_ignore_re=".*node_modules.*"

