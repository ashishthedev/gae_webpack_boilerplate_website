# Development setup
1. On mac, run ./run_mac_dev_server.sh
2. In vagrant, run ./run_vagrant_npm_build_dev_scripts.sh
3. Open http://localhost:8052 in browser

# Hwo to push live changes
1. In vagrant, run ./run_vagrant_live_update.sh

# How to produce on off production build
1/ In vagrant, run ./_run_vagrant_npm_build_production_scripts.sh
