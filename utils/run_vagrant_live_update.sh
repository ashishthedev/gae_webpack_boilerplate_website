#!/bin/bash

set -euxo pipefail

if [ ! -d /vagrant ]; then
	echo "This script is meant to be run only in vagrant environment."
	exit 1
fi

sudo mount --bind /vagrant_node_modules /vagrant/website/node_modules
cd /vagrant/website && npm run build-production

# https://cloud.google.com/appengine/docs/standard/python/config/appref
cd /vagrant/website && gcloud app deploy --quiet --project XXXXXXXXXXXXXXXXX -v "live" 
